import 'package:flutter/material.dart';
import 'package:paye_ton_kawa/widgets/product_entry.dart';

import '../models/Product.dart';

class ProductList extends StatelessWidget {
  final List<Product> products;

  const ProductList({
    Key? key,
    required this.products,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(
            height: 5.0,
          ),
          ...(products.map((Product product) {
            return Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
              child: ProductEntry(product: product),
            );
          }).toList()),
          const SizedBox(
            height: 5.0,
          ),
        ],
      ),
    );
  }
}
