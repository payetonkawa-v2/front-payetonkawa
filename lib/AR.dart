import 'package:augmented_reality_plugin/augmented_reality_plugin.dart';
import 'package:flutter/material.dart';

class AugmentedRealityView extends StatefulWidget {
  static const String id = 'ar';
  const AugmentedRealityView({Key? key}) : super(key: key);

  @override
  _AugmentedRealityViewState createState() => _AugmentedRealityViewState();
}

class _AugmentedRealityViewState extends State<AugmentedRealityView> {
  @override
  Widget build(BuildContext context) {
    return AugmentedRealityPlugin('assets/images/MachineCafe.png');
  }
}
