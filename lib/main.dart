import 'package:flutter/material.dart';
import 'package:paye_ton_kawa/productlist.dart';
import 'login.dart';
import 'qrcode.dart';
import 'ar.dart';
import 'package:camera/camera.dart';

List<CameraDescription> allCameras = [];
Future<void> main() async {
  try {
    WidgetsFlutterBinding.ensureInitialized();
    allCameras = await availableCameras();
  } on CameraException catch (errorMessage) {
    print(errorMessage.description);
  }
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: Login.id,
        routes: {
          Login.id: (context) => Login(
              avatarImage: 'assets/images/avatar.png', navigatePage: () {}),
          Products.id: (context) => Products(),
          QrCode.id: (context) => const QrCode(),
          AugmentedRealityView.id: (context) => AugmentedRealityView(),
        });
  }
}
